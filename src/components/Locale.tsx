import styled from "styled-components";
import { format } from "url";
import { withNamespaces } from "../i18n";

const CurrentLocale = styled.span`
  margin-left: 8px;
`;
const LinkToLocale = styled.a`
  margin-left: 8px;
`;

const Locale = ({ targetLocale, i18n, host, currentUrl }) =>
  targetLocale === i18n.language ? (
    <CurrentLocale key={targetLocale}>{targetLocale}</CurrentLocale>
  ) : (
    <LinkToLocale
      key={targetLocale}
      href={format({ protocol: "https:", ...currentUrl, hostname: host })}
    >
      {targetLocale}
    </LinkToLocale>
  );

export default withNamespaces()(Locale as any) as any;
