import styled from "styled-components";
import { Trans } from "../i18n";

const Wrapper = styled.div`
  position: absolute;
  bottom: 12px;
  right: 25px;
`;

export default () => (
  <Wrapper>
    <a href="https://gitlab.com/kachkaev/website">
      <Trans i18nKey="common:websiteSourceCode" />
    </a>
  </Wrapper>
);
