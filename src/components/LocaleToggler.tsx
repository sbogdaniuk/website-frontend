import styled from "styled-components";
import Locale from "./Locale";

const Wrapper = styled.div`
  position: absolute;
  top: 12px;
  right: 25px;
`;

const LocaleToggler = ({ hostsByLocale, url }) => (
  <Wrapper>
    <Locale targetLocale="en" host={hostsByLocale["en"]} currentUrl={url} />{" "}
    <Locale targetLocale="ru" host={hostsByLocale["ru"]} currentUrl={url} />
  </Wrapper>
);

export default LocaleToggler;
