import { Fragment } from "react";
import styled from "styled-components";
import { Trans } from "../i18n";

const Bold = styled.span`
  font-weight: bold;
`;

export default ({ profileName }: { profileName: string }) => (
  <Fragment>
    <Bold>
      <Trans i18nKey={`profiles.${profileName}.name`} />
    </Bold>
    :{" "}
  </Fragment>
);
