import { ReactNode } from "react";
import styled from "styled-components";
import { Trans } from "../i18n";

const KeyProfileDescriptionWrapper = styled.div`
  opacity: 0.55;
  white-space: nowrap;
`;

const KeyProfileDescription = ({
  profilesByName,
  profileName,
  children,
}: {
  profilesByName: any;
  profileName: string;
  children?: ReactNode;
}) => {
  const profile = profilesByName[profileName];
  if (!profile) {
    return <KeyProfileDescriptionWrapper>&nbsp;</KeyProfileDescriptionWrapper>;
  }
  return (
    <KeyProfileDescriptionWrapper>
      <Trans
        i18nKey={`profiles.${profileName}.description`}
        values={profile.info || {}}
      >
        {children}
      </Trans>
    </KeyProfileDescriptionWrapper>
  );
};

export default KeyProfileDescription;
