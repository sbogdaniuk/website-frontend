import * as compression from "compression";
import * as express from "express";
import * as next from "next";
import * as nextI18NextMiddleware from "next-i18next/middleware";
import { join } from "path";
import { format, parse } from "url";
import { env } from "./config";
import { nextI18next } from "./i18n";

const app = next({
  dir: __dirname,
  conf:
    env.NODE_ENV === "production"
      ? { distDir: "../.next", poweredByHeader: false }
      : undefined,
  dev: env.NODE_ENV !== "production",
});
const handle = app.getRequestHandler();

const rootStaticFiles = [
  "/browserconfig.xml",
  "/favicon.ico",
  "/manifest.json",
  "/robots.txt",
];
(async () => {
  await app.prepare();
  const server = express();
  nextI18NextMiddleware(nextI18next, app, server);

  server.use(compression());

  // redirect legacy CV links
  server.get(
    ["/cv", "/cv_uk", "/cv/uk", "/CVs/Alexander_Kachkaev_CV_UK.pdf"],
    (_, res) => {
      res.redirect("https://www.linkedin.com/in/kachkaev");
    },
  );

  // handle any other requests
  server.get("*", (req, res) => {
    const { pathname, query } = parse(req.url, true);

    if (rootStaticFiles.indexOf(pathname) > -1) {
      const path = join(__dirname, "static", pathname);
      return app.serveStatic(req, res, path);
    }

    if (
      pathname.length > 1 &&
      pathname.slice(-1) === "/" &&
      pathname.indexOf("/_next/") !== 0
    ) {
      return res.redirect(
        format({
          pathname: pathname.slice(0, -1),
          query,
        }),
      );
    }
    (req as any).hostsByLocale = {
      en: env.HOST_EN,
      ru: env.HOST_RU,
    };
    (req as any).gaTrackingId = env.GA_TRACKING_ID;
    (req as any).graphqlUri = env.GRAPHQL_URI;
    (req as any).nodeEnv = env.NODE_ENV;
    return handle(req, res);
  });

  server.listen(env.PORT, (err) => {
    if (err) {
      throw err;
    }
    console.log(`> Ready on port ${env.PORT}`);
  });
})();
