import ApolloClient from "apollo-client";
import App, { Container } from "next/app";
import React from "react";
import { ApolloProvider } from "react-apollo";
import styled from "styled-components";
import LinkToSources from "../components/LinkToSource";
import LocaleToggler from "../components/LocaleToggler";
import { appWithTranslation } from "../i18n";
import "../lib/pageEvents";
import withApolloClient from "../lib/withApolloClient";
import "../styles/index.css";

const Wrapper = styled.div`
  display: table;
  width: 100%;
  height: 100%;
  position: relative;
`;

class MyApp extends App<{ apolloClient: ApolloClient<{}> }> {
  public static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    const { hostsByLocale } = ctx.req || (window as any).__NEXT_DATA__.props;

    return {
      hostsByLocale,
      pageProps: { ...pageProps, hostsByLocale },
    };
  }

  public render() {
    const { Component, pageProps, apolloClient } = this.props;
    return (
      <Container>
        <ApolloProvider client={apolloClient}>
          <Wrapper>
            <LocaleToggler {...pageProps} />
            <Component {...pageProps} />
            <LinkToSources />
          </Wrapper>
        </ApolloProvider>
      </Container>
    );
  }
}

export default withApolloClient(appWithTranslation(MyApp));
