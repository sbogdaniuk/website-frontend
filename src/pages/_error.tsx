import Head from "next/head";
import Link from "next/link";
import Explanation from "../components/Explanation";
import H1 from "../components/H1";
import VerticallyCentered from "../components/VerticallyCentered";
import { Trans, withNamespaces } from "../i18n";

const page = withNamespaces(["_error", "common"])(({ t }) => (
  <VerticallyCentered>
    <Head>
      <title>{t("title")}</title>
    </Head>
    <H1 error={true}>{t("h1")}</H1>
    <Explanation>
      <Trans i18nKey="explanation">
        <a href="mailto:alexander@kachkaev.ru">email</a>
      </Trans>
      <br />
      <br />
      <Link href="/">
        <a>{t("common:signature")}</a>
      </Link>
    </Explanation>
  </VerticallyCentered>
));

page.getInitialProps = () => {
  return {
    namespacesRequired: ["_error", "common"],
  };
};

export default page;
