import * as ICU from "i18next-icu";
import en from "i18next-icu/locale-data/en";
import ru from "i18next-icu/locale-data/ru";
import * as NextI18next from "next-i18next";

// tslint:disable:no-var-requires

const use: any[] = [];
const icu = new ICU({
  formats: {
    number: {
      PRICE: {
        minimumFractionDigits: 2,
        useGrouping: false,
      },
    },
  },
});
icu.addLocaleData(ru);
icu.addLocaleData(en);
use.push(icu);

let detectionOrder: string[] = [];
if (typeof window === "undefined") {
  const { env } = require("./config");
  const i18nextMiddleware = require("i18next-express-middleware");
  const languageDetector = new i18nextMiddleware.LanguageDetector(null, {
    order: ["enforcedLocale", "languageByDomain"],
  });

  languageDetector.addDetector({
    name: "enforcedLocale",
    lookup: () => env.ENFORCED_LOCALE,
    cacheUserLanguage: () => {
      /**/
    },
  });

  languageDetector.addDetector({
    name: "languageByDomain",
    lookup: (opts) => {
      const hostWithoutPort = (opts.headers.host || "").replace(/\:\d+$/, "");
      return hostWithoutPort === env.HOST_RU ? "ru" : "en";
    },
    cacheUserLanguage: () => {
      /**/
    },
  });
  use.push(languageDetector);
  detectionOrder = ["enforcedLocale", "languageByDomain"];
}

export const nextI18next = new NextI18next({
  defaultLanguage: "en",
  otherLanguages: ["ru"],
  localePath: "./locales",
  keySeparator: "###",
  use,
  detection: {
    order: detectionOrder,
  },
});

export const appWithTranslation = nextI18next.appWithTranslation;
export const Trans = nextI18next.Trans;
export const withNamespaces = nextI18next.withNamespaces;
